import time

import nerdvision

from simple_test import SimpleTest

nerdvision.start()

def main():
    ts = SimpleTest("This is a test")
    while True:
        try:
            ts.message(ts.new_id())
        except Exception as e:
            nerdvision.capture_exception()
            print(e)
            ts.reset()

        time.sleep(0.1)


if __name__ == '__main__':
    main()
