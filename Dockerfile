FROM python:3
MAINTAINER Ben Donnelly <ben_donnelly@intergral.com>

ENV NV_NAME=python-test-app
ENV NV_REPO_URL=https://gitlab.com/nerd-vision/test-apps/python/python

# Add our service
ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt

ENTRYPOINT ["python", "src/main.py"]
